const axios = require('axios');

const genre_reducer = (map, pack_model) => {
  const genres = pack_model.genres;

  // Iterate over each genre associated with the pack
  genres.forEach((genre) => {
    if (map[genre] === undefined) {
      // If the genre does not yet exist, initialize it with an empty list.
      map[genre] = [];
    }

    map[genre].push(pack_model);
  });

  return map;
};

axios
  .get('https://api.ampifymusic.com/packs')
  .then((res) => {
    const pack_models = res.data.packs;

    const genres = pack_models.reduce(genre_reducer, new Object());

    console.info(
      '\x1b[40m\033[1m\x1b[32m',
      '📗: Listing all genres.',
      '\x1b[0m'
    );
    console.table(Object.keys(genres));

    console.info(
      '\x1b[40m\033[1m\x1b[32m',
      '📗: Listing all packs in the hip-hop genre.',
      '\x1b[0m'
    );
    console.table(genres['hip-hop'], ['id', 'name']);
  })
  .catch((err) => {
    // Catch any errors, log them to the console.
    console.error(err);
  });

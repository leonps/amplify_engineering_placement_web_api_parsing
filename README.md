# Ampify Engineering Placement - Web Api Parsing - Leon Paterson-Stephens

> Simple script written in JavaScript (**Node.js ®**) to pull information about 'packs' from the Amplify Music web api and subsequently sort the packs into genres.

## Prerequisites

This project has been built and tested using NodeJS (**v12.18.4**) and Yarn (**v1.22.5**).
To make sure you have them available on your machine, try running the following command.

```sh
$ node -v && yarn -v
v12.18.4
v1.22.5
```

## Table of contents

- [Web Api Parsing](#project-name)

- [Prerequisites](#prerequisites)

- [Table of contents](#table-of-contents)

- [Installation](#installation)

- [References](#references)

- [Usage](#usage)

- [Running the app](#serving-the-app)

## Installation

**BEFORE YOU INSTALL:** please read the [prerequisites](#prerequisites)

Start with cloning this repo on your local machine:

> Use the --recurse-submodules argument to clone the amplify-prettier-config repo if you intend to make use of Prettier.

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/Focusrite-Novation/ampify-prettier-config)

```sh
$ git clone --recurse-submodules -j8 https://leonps@bitbucket.org/leonps/amplify_engineering_placement_web_api_parsing.git
```

Install required packages using Yarn:

```sh
$ yarn install
```

## References

- https://github.com/axios/axios
- https://github.com/Focusrite-Novation/ampify-prettier-config

## Usage

### Running the app

```sh
$ yarn start
```
